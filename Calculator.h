#pragma once

#include "Row.h"
#include "Stack.h"
#include "CalcNode.h"

#include <string>

class Calculator {
	public:
		static double calculate(std::string exp);
	private: 
		// algorithm methods
		static CalcNode* m_create_tree(std::string exp);
		static CalcNode* m_get_node(Row<CalcNode*>*& nodes, std::string exp);
		static CalcNode*& m_concat_nodes(Row<CalcNode*>*& sub_nodes, Row<std::string>*& values, Row<Operation>*& actions);
		
		// helper methods
		static Row<Operation>* m_get_operations(const std::string& exp);
		static Row<std::string>* m_get_values(const std::string& exp);
		static std::string Calculator::m_remove_first(std::string str, const std::string& sub_str);
		static int m_get_int(const std::string& str);
		static bool m_is_action(const char& c);
};