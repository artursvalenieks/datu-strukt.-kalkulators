#pragma once

#define NULL 0

enum Operation {
	ADD = '+',
	SUBTRACT = '-',
	DEVIDE = '/',
	MULTIPLY = '*',
	NONE = ' '
};

struct CalcNode {
	private:
		const int m_value;
		const Operation m_operation;

		CalcNode* left_branch;
		CalcNode* right_branch;

		double m_operate(double left_value, double right_value) const;

	public:
		CalcNode(int value);
		CalcNode(Operation operation);
		~CalcNode();
		void set_left_branch(CalcNode* node);
		void set_right_branch(CalcNode* node);
		double get_value() const;
};