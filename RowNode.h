#ifndef ROWNODE_H
#define ROWNODE_H

template <typename Type>
struct RowNode{
public:
	RowNode() : next(this) {}
	RowNode(const RowNode<Type>& node) : m_value(node.m_value) {}
	RowNode(const Type& m_value, RowNode* next)
		: m_value(m_value), next(next) {}

	Type m_value;
	RowNode *next;
};

#endif // NODE_H
