#ifndef STACKNODE_N
#define STACKNODE_H

template <typename Type>
struct StackNode{
public:
	StackNode() : previous(0){}
	StackNode(const StackNode<Type>& node) : m_value(node.m_value) {}
	StackNode(const Type& m_value, StackNode* previous)
		: m_value(m_value), previous(previous) {}

	Type m_value;
	StackNode *previous;
};

#endif // NODE_H
