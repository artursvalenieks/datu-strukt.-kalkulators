#pragma once

#include "BinaryNode.h"

template <class Type>
class BinaryTree {
	private:
		BinaryNode<Type>* root;

	public:
		BinaryTree() : root(NULL) {}

		~BinaryTree() {
			if (root) {
				root->~BinaryNode();
				root = NULL;
			}
		}

		void MakeEmpty() {
			if (root) {
				root->~BinaryNode();
				root = NULL;
			}
		}

		bool IsEmpty() const {
			return (root == NULL) ? true : false;
		}

		int Size() const {
			return (root) ? root->size() : NULL;
		}

		void RetrieveItem(const Type& item, bool& found) const {
			found = (root && root->contains(item)) ? true : false;
		}

		void InsertItem(Type item) {
			root ? root->insert(item) : root = new BinaryNode<Type>(item);
		}

		void DeleteItem(Type item) {
			if (root) root->remove(item);
		}
		
		void GetPredecessor(BinaryNode<Type>*& tree, Type& data) {
			tree = (root) ? root->predecessor(data) : NULL;
		}

		BinaryNode<Type>* getRoot() {
			return root;
		}

		BinaryTree(const BinaryNode<Type>& originalTree) {
			*root = originalTree; // japarbauda
		}

		static void CopyTree(BinaryNode<Type>*& copyTree, const BinaryNode<Type>*originalTree) {

		}

		void operator=(BinaryNode<Type>& originalTree) {
			if (*this != originalTree) {
				if (root) {
					root->~BinaryNode();
					root = 0;
				}
				*root = originalTree;
			}
			return *this;
		}

		void InOrder(BinaryNode<Type>* tree) const {
			if (root) root->inOrder();
		}
		void PostOrder(BinaryNode<Type>* tree) const {
			if (root) root->postOrder();
		}

		void PreOrder(BinaryNode<Type>* tree) const {
			if (root) root->preOrder();
		}

		void Traversals() const {
			if (root) root->traversals();
		}
};