#include "Calculator.h"
#include "BinaryTree.h"

#include <string>
#include <iostream>
#include <fstream>

void testBinaryTree();

int main() {
	std::string exp;
	std::ifstream infile;
	infile.open("expression.txt");

	while (!infile.eof()) 
	{
		getline(infile, exp);
		std::cout << exp << " = " << Calculator::calculate(exp) << std::endl;
	}
	infile.close();


	std::cout << "spiediet enter, lai redzetu parbaudes" << std::endl;
	std::cin.ignore();
	testBinaryTree();


	std::cin.ignore();
	return 0;
}

void testBinaryTree() {
	BinaryTree<int>* tree = new BinaryTree<int>();
	std::cout << "pievienoju elementus 4,1,3" << std::endl;
	tree->InsertItem(4);
	tree->InsertItem(1);
	tree->InsertItem(3);
	std::cout << "Koka izvads ir ";
	tree->Traversals();
	std::cout << "meklejam koka elementu 3. Vai atrada?";
	bool found = false;
	tree->RetrieveItem(3, found);
	std::cout << found << std::endl;
	std::cout << "dzesham to lauka" << std::endl;
	tree->DeleteItem(3);
	std::cout << "Tagad izmers ir:" << tree->Size() << std::endl;
	std::cout << "pievienoju elementus -2" << std::endl;
	std::cout << "Koka izvads ir ";
	tree->InsertItem(-2);
	tree->Traversals();

	std::cout << "izveidojam jaunu koku ar elementiem PFSBHGRYTZW" << std::endl;
	BinaryTree<char>* tree2 = new BinaryTree<char>();
	tree2->InsertItem('P');
	tree2->InsertItem('F');
	tree2->InsertItem('S');
	tree2->InsertItem('B');
	tree2->InsertItem('H');
	tree2->InsertItem('G');
	tree2->InsertItem('R');
	tree2->InsertItem('Y');
	tree2->InsertItem('T');
	tree2->InsertItem('Z');
	tree2->InsertItem('W');
	std::cout << "Koka izvads ir ";
	tree2->Traversals();


	tree2->InsertItem('A');
	tree2->InsertItem('A');

	delete tree;
	delete tree2;
}