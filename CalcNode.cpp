#include "CalcNode.h"


CalcNode::CalcNode(int value) : m_value(value), m_operation(NONE)
{
	left_branch = NULL;
	right_branch = NULL;
}

CalcNode::CalcNode(Operation action) : m_value(-1), m_operation(action)
{
	left_branch = NULL;
	right_branch = NULL;
}

CalcNode::~CalcNode() 
{
	if (left_branch) left_branch->~CalcNode();
	if (right_branch) right_branch->~CalcNode();
}

void CalcNode::set_left_branch(CalcNode* node) 
{
	left_branch = node;
}

void CalcNode::set_right_branch(CalcNode* node) 
{
	right_branch = node;
}

double CalcNode::get_value() const 
{
	double left_value = (left_branch) ? left_branch->get_value() : 0;
	double right_value = (right_branch) ? right_branch->get_value() : 0;
	return m_operate(left_value, right_value);
}

double CalcNode::m_operate(double left_value, double right_value) const 
{
	switch (m_operation)
	{
	case ADD:
		return left_value + right_value;
	case MULTIPLY:
		return left_value * right_value;
	case DEVIDE:
		return left_value / right_value;
	case SUBTRACT:
		return left_value - right_value;
	default:
		return m_value;
	}
}