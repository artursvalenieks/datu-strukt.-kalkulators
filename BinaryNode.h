#pragma once

template <class Type>
class BinaryNode 
{
protected:
	Type value;
	BinaryNode* left_branch;
	BinaryNode* right_branch;

	BinaryNode<Type>*& m_get_branch(const Type& value)
	{
		return this->value > value ? left_branch : right_branch;
	}

	bool m_remove(BinaryNode<Type>& root, const Type& value) 
	{
		if (left_branch && left_branch->value == value) 
		{
			BinaryNode<Type>* branch = left_branch;
			left_branch = 0;
			m_remove_node(&root, branch);
			return true;
		}
		else if (right_branch && right_branch->value == value) 
		{
			BinaryNode<Type>* branch = right_branch;
			right_branch = 0;
			m_remove_node(&root, branch);
			return true;
		}

		BinaryNode<Type>*& child = m_get_branch(value);
		return (child) ? child->m_remove(root, value) : false;
	}

	void m_remove_node(BinaryNode<Type>* root, BinaryNode<Type>*& node) 
	{
		if (node->left_branch)
		{
			root->insert(node->left_branch);
			node->left_branch = 0;
		}

		if (node->right_branch)
		{
			root->insert(node->right_branch);
			node->right_branch = 0;
		}
		node->~BinaryNode();
	}

public:
	BinaryNode(const Type value) : value(value), left_branch(0), right_branch(0) {}

	~BinaryNode()
	{
		if (left_branch) left_branch->~BinaryNode();
		if (right_branch) right_branch->~BinaryNode();
	}

	BinaryNode(BinaryNode<Type>*& node) 
	{
		left_branch = (node->left_branch) ? new BinaryNode<Type>(node->left_branch) : 0;
		right_branch = (node->right_branch) ? new BinaryNode<Type>(node->right_branch) : 0;
		this->value = node->value;
	}

	int size() const 
	{
		int count = left_branch ? left_branch->size() : 0;
		count += right_branch ? right_branch->size() : 0;
		return count + 1;
	}

	bool contains(const Type& value) const 
	{
		if (this->value == value) return true;
		if (left_branch && this->value > value) return left_branch->contains(value);
		if (right_branch && this->value <= value) return right_branch->contains(value);
		return false;
	}

	void insert(const Type& value)
	{
		BinaryNode<Type>*& branch = m_get_branch(value);
		branch ? branch->insert(value) : branch = new BinaryNode<Type>(value);
	}

	void insert(BinaryNode<Type>* node)
	{
		BinaryNode<Type>*& branch = m_get_branch(node->value);
		branch ? branch->insert(node) : branch = (node);
	}

	bool remove(const Type& value) 
	{
		return m_remove(*this, value);
	}

	BinaryNode<Type>* predecessor(const Type& value) 
	{
		if ((left_branch && left_branch->value == value) ||
			(right_branch && right_branch->value == value)) 
		{
			return this;
		}
		else
		{
			BinaryNode<Type>* branch = m_get_branch(value);
			return branch ? branch->predecessor(value) : 0;
		}
	}

	Type get_value() const 
	{
		return value;
	}

	BinaryNode<Type>& operator=(BinaryNode<Type>* tree) 
	{
		if (this != &tree) 
		{
			delete left_branch;
			delete right_branch;
			left_branch = (tree->left_branch) ? new BinaryNode<Type>(tree->left_branch) : 0;
			right_branch = (tree->right_branch) ? new BinaryNode<Type>(tree->right_branch) : 0;
			this->value = tree->value;
		}
		return *this;
	}

	void inOrder() const
	{
		if (left_branch) left_branch->inOrder();
		std::cout << value << " ";
		if (right_branch) right_branch->inOrder();
	}

	void postOrder() const 
	{
		if (left_branch) left_branch->postOrder();
		if (right_branch) right_branch->postOrder();
		std::cout << value << " ";
	}

	void preOrder() const 
	{
		std::cout << value << " ";
		if (left_branch) left_branch->preOrder();
		if (right_branch) right_branch->preOrder();
	}

	void traversals() const
	{
		std::cout << std::endl << "in order: ";
		inOrder();
		std::cout << std::endl << "post order: ";
		preOrder();
		std::cout << std::endl << "pre order: ";
		postOrder();
		std::cout << std::endl;
	}
};
