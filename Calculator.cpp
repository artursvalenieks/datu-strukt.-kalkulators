#include "Calculator.h"

double Calculator::calculate(std::string exp) 
{
	CalcNode* result_node = m_create_tree(exp);
	return result_node->get_value();
}

CalcNode* Calculator::m_create_tree(std::string exp) 
{
	Row<CalcNode*>* nodes = new Row<CalcNode*>; // holds same level nodes e.g. [(4+5),(7/2)]
	Stack<int> bracket_indexes; // holds index of bracket openings in string

	for (int curr = 0; curr < exp.length(); curr++) 
	{
		if (exp[curr] == '(') 
			bracket_indexes.push(curr); // adds openning bracket index in stack
		else if (exp[curr] == ')') 
		{	// is closes first brackets then creates substring from - to index and creates and calls itself
			if (bracket_indexes.size() == 1) 
			{	
				int start = bracket_indexes.pop() + 1;
				std::string sub_str = exp.substr(start, curr - start); // creates string from open to close bracket
				nodes->ennqueue(m_create_tree(sub_str)); // adds same level nodes
				exp = m_remove_first(exp, sub_str); // removes sub expression leaving only - ()
				curr = start;
			}
			else 
			{	// removes opening bracket index if it is not first openning bracket index
				bracket_indexes.pop();
			}
		}
	}
	return m_get_node(nodes, exp); // returns one node(usefull if there is many nodes)
}

CalcNode* Calculator::m_get_node(Row<CalcNode*>*& nodes, std::string exp) 
{
	Row<std::string>* values = m_get_values(exp); // retrieves values from expression [4,5,(),0]
	Row<Operation>* operations = m_get_operations(exp); // retrieves operations from expression [+-*/]
	// if has value and operations creates new node else returns first node
	return (values->size() && operations->size()) ? m_concat_nodes(nodes, values, operations) : nodes->dequeue(); 
}

CalcNode*& Calculator::m_concat_nodes(Row<CalcNode*>*& sub_nodes, Row<std::string>*& values, Row<Operation>*& operations)
{
	CalcNode* node = new CalcNode(operations->dequeue()); // creates new root node with action

	if (values->front() == "()") // if left branch has sub node
	{
		node->set_left_branch(sub_nodes->dequeue()); // retrieves sub node
		values->dequeue(); // removes brackets from row
	}
	else 
	{
		node->set_left_branch(new CalcNode(m_get_int(values->dequeue()))); // creates left branch
	}

	if (operations->isEmpty()) // does not have multiple same level operations
	{

		if (values->front() == "()")  // if right branch has sub node
		{
			node->set_right_branch(sub_nodes->dequeue()); // retrievs sub node
			values->dequeue(); // removes brackets from row
		}
		else 
		{
			node->set_right_branch(new CalcNode(m_get_int(values->dequeue()))); // creates right branch
		}

	}
	else { // has multiple same level operations
		node->set_right_branch(m_concat_nodes(sub_nodes, values, operations)); // creates new node with branches
	}
	return node;
}


Row<std::string>* Calculator::m_get_values(const std::string& str) 
{
	const std::string & delimiters = "+-/*";
	Row<std::string>* sub_str_row = new Row < std::string > ;
	int start = 0;
	int pos = str.find_first_of(delimiters, start);

	while (pos != std::string::npos) 
	{
		if (pos != start) 
			sub_str_row->ennqueue(str.substr(start, pos - start));
		start = pos + 1;
		pos = str.find_first_of(delimiters, start);
	}

	if (start < str.length())
		sub_str_row->ennqueue(str.substr(start, str.length() - start));

	return sub_str_row;
}


Row<Operation>* Calculator::m_get_operations(const std::string& exp)
{
	Row<Operation>* ret = new Row<Operation>;
	for (int i = 0; i < exp.length(); i++) 
		if (m_is_action(exp[i])) ret->ennqueue((Operation)exp[i]);
	return ret;
}


std::string Calculator::m_remove_first(std::string str,const std::string& sub_str) 
{
	int pos = str.find(sub_str, 0);
	return (std::string::npos) ? str.erase(pos, sub_str.length()) : str;
}

int Calculator::m_get_int(const std::string& str) 
{
	return atoi(str.c_str());
}

bool Calculator::m_is_action(const char& c)
{
	return (c == ADD || c == MULTIPLY || c == DEVIDE || c == SUBTRACT);
}