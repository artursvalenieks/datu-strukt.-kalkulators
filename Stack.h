#ifndef STACK_H
#define STACK_H

#include "StackNode.h"
#include <iostream>

template <class Type>
class Stack
{
public:
	Stack() {
		m_node = new StackNode<Type>();
	}
	
	Stack(const Stack<Type> &stack) {
		m_node = new StackNode<Type>();
		m_copyStack(stack);
	}
	
	~Stack() {
		empty();
	}

	Stack<Type>& operator=(const Stack<Type>& stack) {
		if (this != &stack) {
			empty();
			m_copyStack(stack);
		}
		return *this;
	}

	void push(const Type& element) {
		m_node = new StackNode<Type>(element, m_node);
	}

	Type pop() {
		if (isNotEmpty()) {
			Type m_value = m_node->m_value;
			StackNode<Type> *oldNode = m_node;
			m_node = m_node->previous;
			delete oldNode;
			return m_value;
		}
		else {
			throw new EmptyStackException;
		}
	}

	Type top() const {
		if (isNotEmpty()) {
			return m_node->m_value;
		}
		else {
			throw new EmptyStackException;
		}
	}

	int size() const {
		StackNode<Type>* runner = m_node;
		int size;
		for (size = 0; runner->previous != 0; size++, runner = runner->previous);
		return size;
	}

	void empty() {
		for (StackNode<Type>* tmpNode = m_node->previous; m_node->previous != 0; tmpNode = m_node->previous) {
			delete m_node;
			m_node = tmpNode;
		}
	}

	bool isEmpty() const {
		return size() == 0;
	}

	bool isNotEmpty() const {
		return !isEmpty();
	}

private:
	StackNode<Type> *m_node;

	void m_copyStack(const Stack<Type> &stack) {
		Stack<StackNode<Type>*> addressStack;
		StackNode<Type>* tempNode = stack.m_node;

		do {
			addressStack.push(tempNode);
			tempNode = tempNode->previous;
		} while (tempNode->previous != 0);

		while (addressStack.isNotEmpty()) {
			tempNode = addressStack.pop();
			push(tempNode->m_value);
		}
	}
};

class EmptyStackException{};
#endif // STACK_H
